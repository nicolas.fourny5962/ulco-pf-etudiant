main :: IO ()

main = do
    putStr "What's your name : "
    texte <- getLine
    if texte /= "" 
        then do
            putStrLn("Hello : " ++ texte)
            main
        else do
            putStrLn("Good bye")
