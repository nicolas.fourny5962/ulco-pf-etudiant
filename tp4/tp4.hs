import Data.Char

fact :: Int -> Int
fact 1 = 1
fact x = x * fact(x-1)

pgcd :: Int -> Int -> Int
pgcd a b 
    | b == 0 = a
    | otherwise = pgcd b (a `mod` b)

loopEcho :: Int -> IO ()
loopEcho 0 = putStrLn "loop terminated"
loopEcho n = do
    putStrLn "> "
    input <- getLine
    if input /= ""
        then do
            putStrLn input
            loopEcho (n-1)
        else putStrLn "empty line"

factTco :: Integer -> Integer -> Integer
factTco 1 acc = acc
factTco n acc = factTco (n-1) (acc*n)

factTco2 :: Integer -> Integer
factTco2 x = aux x 1
    where
        aux 1 acc = acc
        aux n acc = aux (n-1) (acc*n)

myLength :: [a] -> Int
myLength [] = 0
myLength (_:xs) = 1 + myLength xs

myLength' :: [a] -> Int
myLength' l = aux l 0
        where aux [] acc = acc
              aux (_:xs) acc = aux xs (acc+1)

toUpperString :: String -> String
toUpperString "" = ""
toUpperString (x:xs) = toUpper x : toUpperString xs

onlyLetters :: String -> String
onlyLetters [] = []
onlyLetters (x:xs)  
    | isLetter x == True = onlyLetters xs
    | otherwise = onlyLetters xs

mulListe :: Int -> [Int] -> [Int]
mulListe _ [] = []
mulListe y (x:xs) = y * x : mulListe y xs 

selectListe (Int,Int) -> [Int] -> Int
selectListe (_,_) [] = []
selectListe (x,y) (k:v) = if k > x && k < y then selectListe (x,y) v else selectListe (x,y) v