import Text.Read

main = do
    str <- getLine
    let x = readMaybe str :: Maybe Int
    print x