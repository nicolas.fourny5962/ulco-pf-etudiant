import System.Environment

formaterPararite :: Int -> String
formaterPararite x
    | even x = "Pair" 
    | otherwise = "Impaire"

formaterSigne :: Int -> String
formaterSigne x 
    | x == 0 = "nul"
    | x < 0 = "negatif"
    | otherwise = "positif"

borneEtFormate1 :: Double -> String
borneEtFormate1 x =
    show x ++ " -> " ++ show x'
    where x' =
            if x < 0
            then 0
            else if x > 1
                then 1
                else x

borneEtFormate2 :: Double -> String
borneEtFormate2 x =
    show x ++ " > " ++ show x'
    where x' | x < 0 = 0
            | x > 1 = 1
            | otherwise = x

fibo :: Int -> Int
fibo x = case x of
    0 -> 0
    1 -> 1
    _ -> fibo(x-1)+fibo(x-2)
