import Text.Read (readMaybe)

safeSqrt :: Double -> Maybe Double
safeSqrt x =
    if x > 0
    then Just (sqrt x)
    else Nothing

main = do
    print (safeSqrt 16)
    print (safeSqrt (-1))
    str <- getLine
    let x = readMaybe str :: Maybe Int
    print x