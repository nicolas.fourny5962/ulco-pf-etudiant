palindrome :: String -> Bool
palindrome x 
    | reverse(x) == x = True
    | otherwise = False

twice :: [a] -> [a]
twice x = x ++ x;
