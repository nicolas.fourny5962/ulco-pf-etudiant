add2int :: Int -> Int -> Int
add2int x y = x + y

add2Double :: Double -> Double -> Double
add2Double x y = x + y

add2Num :: Num a => a -> a -> a
add2Num x y = x + y

u :: Int
u = 3

v :: Double
v = 2



main = do
    print (add2Num 1 2)
    print (add2Num 2 3.4)
    print (add2Num u u)
    print (add2Num v v )
